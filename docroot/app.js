'use strict';

var app = angular.module('app', ['ngRoute', 'ui.bootstrap', 'board']);

app.config(['$locationProvider', '$routeProvider', '$logProvider', function ($locationProvider, $routeProvider,
    $logProvider) {

    $locationProvider.html5Mode(true).hashPrefix('!');

    $routeProvider
        .when('/', {
            templateUrl: 'components/board/board.html',
            controller: 'BoardCtrl'
        })
        .otherwise({
            templateUrl: 'components/board/board.html',
            controller: 'BoardCtrl'
        });

    $logProvider.debugEnabled(false);
}]);