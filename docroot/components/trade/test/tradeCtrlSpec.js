'use strict';

describe('tradeCtrl', function () {

    var tradeCtrl;
    var $scope;
    var $modalInstance;

    var noResources = {
        brick: 0,
        lumber: 0,
        wheat: 0,
        wool: 0,
        ore: 0
    };

    var playerData = {
        id: 0,
        points: 0,
        resources: {
            brick: 4,
            lumber: 4,
            wool: 2,
            wheat: 2,
            ore: 0
        },
        settlements: [],
        roads: [],
        developmentCards: [],
        color: 'red',
        name: 'ernesto'
    };

    beforeEach(module('app'));

    // mock services
    beforeEach(module(function ($provide) {

        $modalInstance = {
            close: function (item) {
                this.trade = item;
            },
            dismiss: function (type) {
                this.dismissal = type;
            }
        };

        $provide.value('$modalInstance', $modalInstance);
    }));

    // instantiate TradeCtrl
    beforeEach(inject(function ($rootScope, $controller) {
        $scope = $rootScope.$new();

        tradeCtrl = $controller('TradeCtrl', {
            $scope: $scope,
            player: new Player(playerData)
        });
    }));

    describe('offer', function () {

        it('should default to a blank offer', function () {
            expect($scope.offer).toEqual(noResources);
        });

        it('should appropriately increase offer', function () {
            var offerBefore = angular.copy($scope.offer);
            $scope.offerResource('lumber');

            offerBefore.lumber++;
            expect($scope.offer).toEqual(offerBefore);
        });

        it('should not increase offer beyond what the player has', function () {
            $scope.offerResource('ore');
            expect($scope.offer.ore).toBe(0);
        });

        it('should appropriately decrease offer', function () {
            $scope.offer.lumber++;
            $scope.cancelOfferResource('lumber');
            expect($scope.offer.lumber).toBe(0);
        });

        it('should not decrease offer below 0', function () {
            $scope.cancelOfferResource('lumber');
            expect($scope.offer.lumber).toBe(0);
        });
    });

    describe('demand', function () {

        it('should default to a blank demand', function () {
            expect($scope.demand).toEqual(noResources);
        });

        it('should appropriately increase demand', function () {
            var demandBefore = angular.copy($scope.demand);
            $scope.demandResource('lumber');

            demandBefore.lumber++;
            expect($scope.demand).toEqual(demandBefore);
        });

        it('should appropriately decrease demand', function () {
            $scope.demand.lumber++;
            $scope.cancelDemandResource('lumber');
            expect($scope.demand.lumber).toBe(0);
        });

        it('should not decrease demand below 0', function () {
            $scope.cancelDemandResource('lumber');
            expect($scope.demand.lumber).toBe(0);
        });
    });

    describe('bank', function () {

        it('should know if a trade is valid for the bank', function () {
            expect($scope.isValidForBank()).toBe(false);

            $scope.offerResource('lumber');
            $scope.offerResource('lumber');
            $scope.offerResource('lumber');
            $scope.demandResource('ore');
            expect($scope.isValidForBank()).toBe(false);

            $scope.offerResource('lumber');
            expect($scope.isValidForBank()).toBe(true);

            $scope.cancelOfferResource('lumber');
            expect($scope.isValidForBank()).toBe(false);
        });

        it('should not let you trade the bank for the same resource', function () {
            $scope.offerResource('lumber');
            $scope.offerResource('lumber');
            $scope.offerResource('lumber');
            $scope.offerResource('lumber');
            $scope.demandResource('lumber');
            expect($scope.isValidForBank()).toBe(false);
        });
    });

    it('should dismiss the modal when cancel is called', function () {
        $scope.cancel();
        expect($modalInstance.dismissal).toEqual('cancel');
    });

    it('should return a trade when bank is called', function () {
        $scope.offerResource('lumber');
        $scope.offerResource('lumber');
        $scope.offerResource('lumber');
        $scope.offerResource('lumber');
        $scope.demandResource('ore');
        $scope.bank();

        expect($modalInstance.trade.type).toEqual('bank');
        expect($modalInstance.trade.offer.lumber).toEqual(4);
        expect($modalInstance.trade.demand.ore).toEqual(1);
    });

    it('should return a trade when an offer is made', function () {
        $scope.offerResource('lumber');
        $scope.demandResource('ore');
        $scope.makeOffer();

        expect($modalInstance.trade.type).toEqual('offer');
        expect($modalInstance.trade.offer.lumber).toEqual(1);
        expect($modalInstance.trade.demand.ore).toEqual(1);
    });
});