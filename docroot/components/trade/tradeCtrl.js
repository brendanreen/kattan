'use strict';

board.controller('TradeCtrl', ['$scope', '$modalInstance', 'player', function ($scope, $modalInstance, player) {

    $scope.offer = {
        brick: 0,
        lumber: 0,
        wheat: 0,
        wool: 0,
        ore: 0
    };

    $scope.demand = {
        brick: 0,
        lumber: 0,
        wheat: 0,
        wool: 0,
        ore: 0
    };

    $scope.bankResources = {
        brick: 1,
        lumber: 1,
        wheat: 1,
        wool: 1,
        ore: 1
    };

    // work with a copy so we don't need to worry about restoring the player's resources if they cancel
    $scope.player = {};
    $scope.player.resources = angular.copy(player.resources);

    var resources = Object.keys($scope.offer);

    var isValidForBankFlag = false;

    var isValidForBank = function () {
        var equivalentTrade = 0;
        var totalDemand = 0;
        var amount;

        var offeredResources = [];
        var demandedResources = [];

        for (var i = 0; i < resources.length; i++) {
            amount = $scope.offer[resources[i]];

            if (amount > 0) {
                offeredResources.push(resources[i]);
            }

            if (amount % 4 != 0) {
                return false;

            } else {
                equivalentTrade += amount / 4;
            }
        }

        angular.forEach(resources, function (resource) {
            var amount = $scope.demand[resource];

            if (amount > 0) {
                demandedResources.push(resource);
            }

            totalDemand += amount;
        });

        if (angular.equals(offeredResources, demandedResources)) {
            return false;

        } else {
            return equivalentTrade == totalDemand;
        }
    };

    var updateIsValidForBank = function () {
        isValidForBankFlag = isValidForBank();
    };

    $scope.makeOffer = function () {
        $modalInstance.close({
            type: 'offer',
            offer: $scope.offer,
            demand: $scope.demand
        });
    };

    $scope.bank = function () {
        $modalInstance.close({
            type: 'bank',
            offer: $scope.offer,
            demand: $scope.demand
        });
    };

    $scope.isValidForBank = function () {
        return isValidForBankFlag;
    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };

    $scope.offerResource = function (resource) {
        if ($scope.player.resources[resource] > 0) {
            $scope.player.resources[resource]--;
            $scope.offer[resource]++;
            updateIsValidForBank();
        }
    };

    $scope.cancelOfferResource = function (resource) {
        if ($scope.offer[resource] > 0) {
            $scope.offer[resource]--;
            $scope.player.resources[resource]++;
            updateIsValidForBank();
        }
    };

    $scope.demandResource = function (resource) {
        $scope.demand[resource]++;
        updateIsValidForBank();
    };

    $scope.cancelDemandResource = function (resource) {
        if ($scope.demand[resource] > 0) {
            $scope.demand[resource]--;
            updateIsValidForBank();
        }
    };

}]);