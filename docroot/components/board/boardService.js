'use strict';

app.factory('boardService', ['$log', function ($log) {

    return {
        getBoard: function (x, y, r) {
            // don't bother if any params are 0 or negative
            // or weren't even provided
            if (x <= 0 || y <= 0 || r <= 0 || !x || !y || !r) {
                throw new Error('Parameters must be positive');
            }

            else {
                return new Board(x, y, r);
            }
        }
    };
}]);