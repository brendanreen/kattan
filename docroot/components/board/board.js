var Board = function (x, y, r) {

    // don't bother if any params are 0 or negative
    // or weren't even provided
    if (x <= 0 || y <= 0 || r <= 0 || !x || !y || !r) {
        throw new Error('Parameters must be positive');
    }

    this.tiles = {
        0: undefined,
        1: undefined,
        2: [],
        3: [],
        4: [],
        5: [],
        6: [],
        7: [],
        8: [],
        9: [],
        10: [],
        11: [],
        12: []
    };

    this.oceanTiles = [];
    this.plots = [];
    this.paths = [];

    // TODO surely there's a less awful way to do this
    var Tile;
    (function (plots) {

        var getPlot = function (cx, cy) {
            var plot = {
                cx: cx,
                cy: cy,
                r: 16,
                fill: 'lavender',
                stroke: 'grey',
                strokeWidth: 1,
                tiles: []
            };

            var alreadyPlotted = false;
            for (var i = 0; i < plots.length && !alreadyPlotted; i++) {
                if (plot.cx == plots[i].cx && plot.cy == plots[i].cy) {
                    alreadyPlotted = true;
                    break;
                }
            }

            if (alreadyPlotted) {
                return plots[i];

            } else {
                plots.push(plot);
                return plot;
            }
        };

        Tile = function (centerX, centerY, r, terrain, roll) {
            this.terrain = terrain.type;
            this.x = centerX;
            this.y = centerY;
            this.roll = roll;
            this.plots = [];
            this.stroke = 'grey';
            this.strokeWidth =  1;
            this.fill = terrain.color;

            var path = '';
            var angle, x, y;
            for (var i = 0; i < 6; i++) {
                angle = 2 * (Math.PI / 6) * i;
                x = Math.round(centerX + r * Math.cos(angle));
                y = Math.round(centerY + r * Math.sin(angle));
                path += x + ',' + y + ' ';

                if (terrain.type != 'ocean') {
                    this.plots.push(getPlot(x, y));
                }
            }

            this.points = path;
        };

        Tile.prototype = {
            constructor: Tile
        };

    })(this.plots);

    // TODO these colors should be defined in css
    var terrains = {
        desert: {
            type: 'desert',
            color: 'tan'
        },
        ore: {
            type: 'ore',
            color: 'grey'
        },
        brick: {
            type: 'brick',
            color: 'brown'
        },
        lumber: {
            type: 'lumber',
            color: 'green'
        },
        wheat: {
            type: 'wheat',
            color: 'gold'
        },
        wool: {
            type: 'wool',
            color: 'linen'
        },
        ocean: {
            type: 'ocean',
            color: 'lightseagreen'
        }
    };

    var availableTerrains = [
        'desert',
        'ore', 'ore', 'ore',
        'brick', 'brick', 'brick',
        'wool', 'wool', 'wool', 'wool',
        'wheat', 'wheat', 'wheat', 'wheat',
        'lumber', 'lumber', 'lumber', 'lumber'
    ];

    var availableRolls = [ 2, 3, 3, 4, 4, 5, 5, 6, 6, 7, 8, 8, 9, 9, 10, 10, 11, 11, 12 ];

    var getRandomTerrain = function () {
        var index = Math.floor(Math.random() * availableTerrains.length);
        var randomTerrainType = availableTerrains.splice(index, 1);
        return terrains[randomTerrainType];
    };

    var getRandomRoll = function () {
        var index = Math.floor(Math.random() * availableRolls.length);
        return availableRolls.splice(index, 1)[0];
    };

    var addTile = function (tiles, x, y, r, terrain, roll) {
        if (terrain.type == 'desert' && roll != 7) {
            availableRolls.push(roll);
            var index = availableRolls.indexOf(7);
            roll = availableRolls.splice(index, 1)[0];
        }

        else if (roll == 7 && terrain.type != 'desert') {
            availableTerrains.push(terrain.type);
            var index = availableTerrains.indexOf('desert');
            availableTerrains.splice(index, 1);
            terrain = terrains.desert;
        }

        var tile = new Tile(x, y, r, terrain, roll);

        if (tile.terrain == 'desert') {
            tile.hasRobber = true;
        }

        tiles[roll].push(tile);
    };

    var assignNeighbors = function (plots) {

        var pushUnique = function (array, item) {
            if (array.indexOf(item) == -1) {
                array.push(item);
            }
        };

        angular.forEach(plots, function (plotA) {
            plotA.neighbors = plotA.neighbors || [];

            angular.forEach(plots, function (plotB) {
                plotB.neighbors = plotB.neighbors || [];

                if (plotA != plotB) {
                    var dX = Math.abs(plotA.cx - plotB.cx);
                    var dY = Math.abs(plotA.cy - plotB.cy);

                    if (dY == 0 && dX <= 60) {
                        pushUnique(plotA.neighbors, plotB);
                        pushUnique(plotB.neighbors, plotA);
                    }

                    if (dY <= 52 && dX <= 30) {
                        pushUnique(plotA.neighbors, plotB);
                        pushUnique(plotB.neighbors, plotA);
                    }
                }
            });
        });
    };

    var getPaths = function (plots) {
        var paths = [];
        var alreadyAdded;

        var pathsEqual = function (pathA, pathB) {
            return (pathA.plotA.cx == pathB.plotA.cx &&
                pathA.plotA.cy == pathB.plotA.cy &&
                pathA.plotB.cx == pathB.plotB.cx &&
                pathA.plotB.cy == pathB.plotB.cy) ||
                (pathA.plotA.cx == pathB.plotB.cx &&
                    pathA.plotA.cy == pathB.plotB.cy &&
                    pathA.plotB.cx == pathB.plotA.cx &&
                    pathA.plotB.cy == pathB.plotA.cy)
        };

        angular.forEach(plots, function (plotA) {
            angular.forEach(plotA.neighbors, function (plotB) {

                var path = {
                    plotA: plotA,
                    plotB: plotB,
                    fill: 'lavender'
                };

                alreadyAdded = false;

                angular.forEach(paths, function (existingPath) {
                    if (pathsEqual(path, existingPath)) {
                        alreadyAdded = true;
                    }
                });

                if (!alreadyAdded) {
                    paths.push(path);

                    // give the plots a reference to the path
                    path.plotA.paths = path.plotA.paths || [];
                    path.plotA.paths.push(path);
                    path.plotB.paths = path.plotB.paths || [];
                    path.plotB.paths.push(path);
                }
            });
        });

        return paths;
    };

    var w = r * 2;
    var dW = (3 / 4) * w;
    var h = (Math.sqrt(3) / 2) * w;
    var yAdj;
    var grid = [
        [-1, -1, -1, 0, 0, 0, 0],
        [-1, -1, 0, 1, 1, 1, 0],
        [-1, 0, 1, 1, 1, 1, 0],
        [0, 1, 1, 1, 1, 1, 0],
        [0, 1, 1, 1, 1, 0, -1],
        [0, 1, 1, 1, 0, -1, -1],
        [0, 0, 0, 0, -1, -1, -1]
    ];

    // map tiles to the grid
    for (var row = 0; row < grid.length; row++) {
        for (var col = 0; col < grid[row].length; col++) {
            yAdj = y + h * (row + (col / 2) - 0.5);

            if (grid[row][col] == 1) {
                addTile(this.tiles, x + (col * dW), yAdj, r, getRandomTerrain(), getRandomRoll());

            } else if (grid[row][col] == 0) {
                this.oceanTiles.push(new Tile(x + (col * dW), yAdj, r, terrains.ocean, 0));
            }
        }
    }

    // build a flat array of plots (with no duplicates)
    for (var i = 0; i < Object.keys(this.tiles).length; i++) {
        if (this.tiles[i]) {
            for (var j = 0; j < this.tiles[i].length; j++) {
                for (var k = 0; k < this.tiles[i][j].plots.length; k++) {
                    var plot = this.tiles[i][j].plots[k];
                    var alreadyPlotted = false;
                    for (var l = 0; l < this.plots.length && !alreadyPlotted; l++) {
                        if (plot.cx == this.plots[l].cx && plot.cy == this.plots[l].cy) {
                            alreadyPlotted = true;
                            break;
                        }
                    }
                    if (!alreadyPlotted) {
                        this.plots.push(this.tiles[i][j].plots[k]);
                    }
                }
            }
        }
    }

    // assign plot neighbors
    assignNeighbors(this.plots);

    // get paths between plots
    this.paths = getPaths(this.plots);
};

Board.prototype = {

    constructor: Board

};