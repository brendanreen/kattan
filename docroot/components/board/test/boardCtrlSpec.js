'use strict';

describe('boardCtrl', function () {
    var boardCtrl;
    var $scope;
    var tiles;
    var plotA, plotB;
    var path;

    var $modalInstance = {
        result: {
            then: function (confirmCallback, cancelCallback) {
                this.confirmCallback = confirmCallback;
                this.cancelCallback = cancelCallback;
            }
        },
        close: function (item) {
            this.result.confirmCallback(item);
        },
        dismiss: function (type) {
            this.result.cancelCallback(type);
        }
    };

    var startingResources = {
        brick: 4,
        lumber: 4,
        wheat: 2,
        wool: 2,
        ore: 0
    };

    var noResources = {
        brick: 0,
        lumber: 0,
        wheat: 0,
        wool: 0,
        ore: 0
    };

    var oneOre = angular.copy(startingResources);
    oneOre.ore = 1;

    var twoOre = angular.copy(startingResources);
    twoOre.ore = 2;

    var playerData = {
        id: 0,
        points: 0,
        resources: {
            brick: 4,
            lumber: 4,
            wool: 2,
            wheat: 2,
            ore: 0
        },
        settlements: [],
        roads: [],
        developmentCards: [],
        color: 'red',
        name: 'ernesto'
    };

    var doSettlement = function (player, plot) {
        expect($scope.currentPlayer).toEqual(player);
        expect($scope.showPlots).toBe(true);
        expect($scope.showPaths).toBe(false);
        $scope.build(plot);
    };

    var doRoad = function (player, path) {
        expect($scope.currentPlayer).toEqual(player);
        expect($scope.showPlots).toBe(false);
        expect($scope.showPaths).toBe(true);
        $scope.buildRoad(path);
    };

    var cleanup = function (plot, path) {
        // reset plot and path so we can re-use them
        plot.hasSettlement = false;
        path.hasRoad = false;
    };

    var doTurn = function (player, plot, path) {
        doSettlement(player, plot);
        doRoad(player, path);
        cleanup(plot, path);
    };

    beforeEach(module('app'));

    // mock services
    // TODO use spies for this instead
    beforeEach(module(function ($provide) {
        plotA = {
            cx: 0,
            cy: 0
        };

        plotB = {
            cx: 1,
            cy: 1
        };

        var path = {
            plotA: plotA,
            plotB: plotB
        };

        var tile = {
            plots: [plotA, plotB],
            fill: 'grey',
            terrain: 'ore'
        };

        var desertTile = {
            hasRobber: true
        };


        tiles = {
            2: [tile],
            7: [desertTile]
        };

        var boardService = {
            getBoard: function () {
                return {
                    tiles: tiles,
                    oceanTiles: [],
                    plots: [plotA, plotB],
                    paths: [path]
                }
            }
        };

        var buildService = {
            buildSettlement: function (player, plot, isStartingPlacement) {
                if (plot.hasSettlement) {
                    throw new Error();

                } else {
                    plot.hasSettlement = true;
                    player.settlements.push(plot);
                }
            },

            buildRoad: function (player, path) {
                path.hasRoad = true;
                player.roads.push(path);
            },

            buildCity: function (player, plot) {
                plot.hasCity = true;
            }
        };

        $provide.value('boardService', boardService);
        $provide.value('buildService', buildService);
    }));

    beforeEach(inject(function ($modal, $q, gameService) {
        spyOn($modal, 'open').andReturn($modalInstance);

        var gameDeferred = $q.defer();

        var gameData = {
            players: [new Player(playerData), new Player(playerData), new Player(playerData)]
        };

        var game = new Game(gameData);

        gameDeferred.resolve(game);
        spyOn(gameService, 'newGame').andReturn(gameDeferred.promise);
    }));

    // instantiate BoardCtrl
    beforeEach(inject(function ($rootScope, $controller) {
        $scope = $rootScope.$new();
        boardCtrl = $controller('BoardCtrl', {
            $scope: $scope
        });
        $scope.$apply();
    }));

    it('should initialize tiles and graph', function () {
        expect($scope.tiles).toEqual(tiles);
        expect($scope.oceanTiles).toEqual([]);
        expect($scope.graph.width).toBeDefined();
        expect($scope.graph.height).toBeDefined();
    });

    describe('starting placement', function () {

        it('should follow the starting placement sequence', function () {
            var plot = $scope.plots[0];
            var path = $scope.paths[0];

            expect($scope.isStartingPlacement()).toBe(true);

            doTurn($scope.players[0], plot, path);
            doTurn($scope.players[1], plot, path);
            doTurn($scope.players[2], plot, path);

            expect($scope.players[2].resources).toEqual(startingResources);
            doTurn($scope.players[2], plot, path);
            expect($scope.players[2].resources).toEqual(oneOre);

            expect($scope.players[1].resources).toEqual(startingResources);
            doTurn($scope.players[1], plot, path);
            expect($scope.players[1].resources).toEqual(oneOre);
            expect($scope.players[2].resources).toEqual(oneOre);

            expect($scope.players[0].resources).toEqual(startingResources);
            doTurn($scope.players[0], plot, path);
            expect($scope.players[0].resources).not.toEqual(startingResources);
            expect($scope.players[0].resources).toEqual(oneOre);
            expect($scope.players[1].resources).toEqual(oneOre);
            expect($scope.players[2].resources).toEqual(oneOre);

            expect($scope.currentPlayer).toEqual($scope.players[0]);
            expect($scope.isStartingPlacement()).toBe(false);
        });

        it('should not allow you to roll during starting placement', function () {
            expect(function () {
                $scope.rollDice();
            }).toThrow(new Error('Cannot roll during starting placement'));
        });

        it('should not allow you to end turn during starting placement', function () {
            expect(function () {
                $scope.endTurn();
            }).toThrow(new Error('Cannot end turn during starting placement'));
        });
    });

    describe('error messages', function () {

        it('should set an error if it fails to build a settlement', function () {
            var plot = $scope.plots[0];
            $scope.build(plot);
            expect($scope.errorMessage).toBeUndefined();

            $scope.build(plot);
            expect($scope.errorMessage).toBeDefined();
        });

        it('should set an error if it fails to build a road', function () {
            expect($scope.errorMessage).toBeUndefined();
            $scope.buildRoad(path);
            expect($scope.errorMessage).toBeDefined();
        });

        it('should clear the error message', function () {
            $scope.buildRoad(path);
            expect($scope.errorMessage).toBeDefined();
            $scope.clearError();
            expect($scope.errorMessage).toBeFalsy();
        });
    });

    describe('normal turns', function () {

        beforeEach(function () {
            // skip starting placement
            $scope.isStartingPlacement = function () {
                return false;
            };
            $scope.isStartingResource = function () {
                return false;
            };
        });

        describe('rolling', function () {

            it('should not allow you to roll twice', function () {
                // mock the roll function
                $scope.getRoll = function () {
                    return 2;
                };

                $scope.rollDice();
                expect($scope.roll).toBe(2);

                // mock the roll function again
                $scope.getRoll = function () {
                    return 3;
                };

                $scope.rollDice();
                expect($scope.roll).toBe(2);
            });

            it('should roll a random number and award resources', function () {
                $scope.rollDice();
                expect($scope.roll).toBeDefined();
                expect($scope.roll).toBeGreaterThan(1);
                expect($scope.roll).toBeLessThan(13);

                // don't generate resources if nothing has been built
                angular.forEach($scope.players, function (player) {
                    expect(player.resources).toEqual(startingResources);
                });

                // mock the roll function
                var roll = 2;
                $scope.getRoll = function () {
                    return roll;
                };

                // build a settlement on a plot adjacent to the mock-rolled tile
                $scope.building = true;
                $scope.build($scope.tiles[roll][0].plots[0]);

                $scope.rolled = false;
                $scope.rollDice();
                expect($scope.currentPlayer.resources).toEqual(oneOre);

                // make sure the other players didn't get anything
                angular.forEach($scope.players, function (player) {
                    if (player != $scope.currentPlayer) {
                        expect(player.resources).toEqual(startingResources);
                    }
                });
            });

            it('should award double resources for a city', function () {
                $scope.rollDice();
                expect($scope.roll).toBeDefined();
                expect($scope.roll).toBeGreaterThan(1);
                expect($scope.roll).toBeLessThan(13);

                // don't generate resources if nothing has been built
                angular.forEach($scope.players, function (player) {
                    expect(player.resources).toEqual(startingResources);
                });

                // mock the roll function
                var roll = 2;
                $scope.getRoll = function () {
                    return roll;
                };

                // build a settlement on a plot adjacent to the mock-rolled tile
                $scope.building = true;
                $scope.build($scope.tiles[roll][0].plots[0]);

                $scope.building = true;
                $scope.buildingCity = true;
                $scope.build($scope.tiles[roll][0].plots[0]);

                $scope.rolled = false;
                $scope.rollDice();
                expect($scope.currentPlayer.resources).toEqual(twoOre);

                // make sure the other players didn't get anything
                angular.forEach($scope.players, function (player) {
                    if (player != $scope.currentPlayer) {
                        expect(player.resources).toEqual(startingResources);
                    }
                });
            });

            it('should not award resources from a tile if the robber is there', function () {
                // mock the roll function
                var roll = 2;
                $scope.getRoll = function () {
                    return roll;
                };

                var tile = $scope.tiles[roll][0];

                $scope.building = true;
                $scope.build(tile.plots[0]);

                tile.hasRobber = true;
                $scope.rollDice();
                expect($scope.currentPlayer.resources).toEqual(startingResources);
            });

            it('should set the fill of a rolled tile to purple and reset it later', function () {
                // mock the roll function
                var roll = 2;
                $scope.getRoll = function () {
                    return roll;
                };

                var oldFill = angular.copy($scope.tiles[roll][0].fill);
                expect(oldFill).not.toEqual('purple');

                $scope.rollDice();
                expect($scope.tiles[roll][0].fill).toEqual('purple');
                $scope.endTurn();

                $scope.getRoll = function () {
                    return roll + 1;
                };
                $scope.rollDice();
                expect($scope.tiles[roll][0].fill).toEqual(oldFill);
            });
        });

        describe('road building', function () {

            beforeEach(function () {
                $scope.getRoll = function () {
                    return 2;
                };
            });

            it('should not allow you to start building a road before rolling', function () {
                $scope.startBuildingRoad();
                expect($scope.showPaths).toBeFalsy();
                expect($scope.building).toBeFalsy();
            });

            it('should not allow you to start building a road if you have insufficient resources', function () {
                $scope.rollDice();
                $scope.currentPlayer.resources = noResources;
                $scope.startBuildingRoad();
                expect($scope.showPaths).toBeFalsy();
                expect($scope.building).toBeFalsy();
            });

            it('should allow you to start building a road after you have rolled and if you have sufficient resources',
                function () {
                    $scope.rollDice();
                    $scope.startBuildingRoad();
                    expect($scope.showPaths).toBe(true);
                    expect($scope.building).toBe(true);
                });

            it('should reset the board if road building is cancelled', function () {
                $scope.rollDice();
                $scope.startBuildingRoad();
                $scope.cancelBuild();
                expect($scope.showPaths).toBe(false);
                expect($scope.building).toBe(false);
            });

            it('should reset the board if road is built', function () {
                $scope.rollDice();
                $scope.startBuildingRoad();
                $scope.buildRoad($scope.paths[0]);
                expect($scope.showPaths).toBe(false);
                expect($scope.building).toBe(false);
            });
        });

        describe('settlement building', function () {

            beforeEach(function () {
                // manually hide plots, since they'll be shown by default during starting placement
                $scope.showPlots = false;

                $scope.getRoll = function () {
                    return 2;
                };
            });

            it('should not allow you to start building a settlement before rolling', function () {
                $scope.startBuildingSettlement();
                expect($scope.showPlots).toBeFalsy();
                expect($scope.building).toBeFalsy();
            });

            it('should not allow you to start building a settlement if you have insufficient resources', function () {
                $scope.rollDice();
                $scope.currentPlayer.resources = noResources;
                $scope.startBuildingSettlement();
                expect($scope.showPlots).toBeFalsy();
                expect($scope.building).toBeFalsy();
            });

            it('should allow you to start building a settlement after you have rolled and if you have sufficient resources',
                function () {
                    $scope.rollDice();
                    $scope.startBuildingSettlement();
                    expect($scope.showPlots).toBe(true);
                    expect($scope.building).toBe(true);
                });

            it('should reset the board if settlement building is cancelled', function () {
                $scope.rollDice();
                $scope.startBuildingSettlement();
                $scope.cancelBuild();
                expect($scope.showPlots).toBe(false);
                expect($scope.building).toBe(false);
            });

            it('should reset the board if settlement is built', function () {
                $scope.rollDice();
                $scope.startBuildingSettlement();
                $scope.build($scope.plots[0]);
                expect($scope.showPlots).toBe(false);
                expect($scope.building).toBe(false);
            });
        });

        describe('city building', function () {

            beforeEach(function () {
                $scope.currentPlayer.resources.ore += 3;
                $scope.currentPlayer.resources.wheat += 2;

                $scope.getRoll = function () {
                    return 2;
                };
            });

            it('should not allow you to start building a settlement before rolling', function () {
                $scope.startBuildingCity();
                expect($scope.building).toBeFalsy();
                expect($scope.buildingCity).toBeFalsy();
            });

            it('should not allow you to start building a city if you have insufficient resources', function () {
                $scope.rollDice();
                $scope.currentPlayer.resources = noResources;
                $scope.startBuildingCity();
                expect($scope.building).toBeFalsy();
                expect($scope.buildingCity).toBeFalsy();
            });

            it('should allow you to start building a city after you have rolled and if you have sufficient resources',
                function () {
                    $scope.rollDice();
                    $scope.startBuildingCity();
                    expect($scope.building).toBe(true);
                    expect($scope.buildingCity).toBe(true);
                });

            it('should reset the board if city building is cancelled', function () {
                $scope.rollDice();
                $scope.startBuildingCity();
                $scope.cancelBuild();
                expect($scope.building).toBe(false);
                expect($scope.buildingCity).toBe(false);
            });

            it('should reset the board if city is built', function () {
                $scope.rollDice();
                $scope.startBuildingSettlement();
                $scope.build($scope.plots[0]);

                expect($scope.building).toBe(false);

                $scope.startBuildingCity();
                $scope.build($scope.plots[0]);

                expect($scope.building).toBe(false);
                expect($scope.buildingCity).toBe(false);
            });
        });

        describe('trading', function () {

            beforeEach(function () {
                $scope.getRoll = function () {
                    return 2;
                };
            });

            it('should not open a trade modal if player has not yet rolled', function () {
                $scope.openTrade();
                expect($scope.tradeModal).not.toBeDefined();
            });

            it('should open a trade modal if the player has rolled', function () {
                $scope.rollDice();
                $scope.openTrade();
                expect($scope.tradeModal).toBeDefined();
            });

            it('should not impact resources if the modal is dismissed', function () {
                var resourcesBefore = angular.copy($scope.currentPlayer.resources);
                $scope.rollDice();
                $scope.openTrade();
                $scope.tradeModal.dismiss('cancel');
                expect($scope.currentPlayer.resources).toEqual(resourcesBefore);
            });

            it('should add/subtract resources appropriately if the trade modal is closed with a valid bank trade',
                function () {
                    var resourcesBefore = angular.copy($scope.currentPlayer.resources);
                    $scope.rollDice();
                    $scope.openTrade();

                    var trade = {
                        type: 'bank',
                        offer: {
                            lumber: 4
                        },
                        demand: {
                            ore: 1
                        }
                    };

                    $scope.tradeModal.close(trade);

                    resourcesBefore.lumber -= 4;
                    resourcesBefore.ore++;
                    expect($scope.currentPlayer.resources).toEqual(resourcesBefore);
                });
        });

        describe('robber', function () {

            beforeEach(function () {
                // mock the roll function
                $scope.getRoll = function () {
                    return 7;
                };
            });

            it('should enable moving of the robber when a 7 is rolled', function () {
                expect($scope.robberMustMove).toBeFalsy();

                $scope.rollDice();
                expect($scope.robberMustMove).toBe(true);
            });

            it('should prevent building, trading, and ending the turn until the robber is moved', function () {
                $scope.rollDice();

                expect(function () {
                    $scope.startBuildingRoad();
                }).toThrow(new Error('Robber must be moved first'));

                expect(function () {
                    $scope.startBuildingSettlement();
                }).toThrow(new Error('Robber must be moved first'));

                expect(function () {
                    $scope.startBuildingCity();
                }).toThrow(new Error('Robber must be moved first'));

                expect(function () {
                    $scope.buyDevelopmentCard();
                }).toThrow(new Error('Robber must be moved first'));

                expect(function () {
                    $scope.openTrade();
                }).toThrow(new Error('Robber must be moved first'));

                expect(function () {
                    $scope.endTurn();
                }).toThrow(new Error('Robber must be moved first'));
            });

            it('should not move the robber to its current tile', function () {
                var desertTile = $scope.tiles[7][0];
                expect(desertTile.hasRobber).toBe(true);

                $scope.rollDice();

                $scope.moveRobber(desertTile);
                expect($scope.robberMustMove).toBe(true);
                expect(desertTile.hasRobber).toBe(true);
            });

            it('should move the robber from one tile to another', function () {
                var tile = $scope.tiles[2][0];
                var desertTile = $scope.tiles[7][0];

                $scope.rollDice();

                $scope.moveRobber(tile);
                expect($scope.robberMustMove).toBe(false);
                expect(tile.hasRobber).toBe(true);
                expect(desertTile.hasRobber).toBe(false);
            });
        });

        it('should reset the nav when turn is ended', function () {
            // mock the roll function
            $scope.getRoll = function () {
                return 2;
            };

            $scope.rollDice();
            $scope.startBuildingSettlement();
            expect($scope.building).toBe(true);

            $scope.endTurn();
            expect($scope.building).toBe(false);

            $scope.currentPlayer.resources.ore += 3;
            $scope.currentPlayer.resources.wheat += 2;
            $scope.rollDice();
            $scope.startBuildingCity();
            expect($scope.building).toBe(true);
            expect($scope.buildingCity).toBe(true);

            $scope.endTurn();
            expect($scope.building).toBe(false);
            expect($scope.buildingCity).toBe(false);
        });

        it('should end the game when a player ends a turn with 10 points', function () {
            expect($scope.winner).toBeUndefined();
            $scope.currentPlayer.points = 10;
            var winner = $scope.currentPlayer;

            $scope.endTurn();
            expect($scope.winner).toEqual(winner);
        });

        it('should end the game when a player ends a turn with more than 10 points', function () {
            expect($scope.winner).toBeUndefined();
            $scope.currentPlayer.points = 11;
            var winner = $scope.currentPlayer;

            $scope.endTurn();
            expect($scope.winner).toEqual(winner);
        });
    });
});