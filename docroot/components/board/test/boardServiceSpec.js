'use strict';

describe('boardService', function () {

    var boardService;

    beforeEach(module('app'));

    beforeEach(inject(function (_boardService_) {
        boardService = _boardService_;
    }));

    it('should gracefully handle bad parameters', function () {
        expect(function () {
            boardService.getBoard(-1, -1, -1);
        }).toThrow(new Error('Parameters must be positive'));

        expect(function () {
            boardService.getBoard();
        }).toThrow(new Error('Parameters must be positive'));
    });

    it('should return a board with valid parameters', function () {
        var board = boardService.getBoard(400, 400, 60);
        expect(board).toBeDefined();
    });
});