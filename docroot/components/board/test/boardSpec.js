'use strict';

describe('board', function () {

    beforeEach(module('app'));

    it('should gracefully handle bad parameters', function () {
        expect(function () {
            new Board(-1, -1, -1);
        }).toThrow(new Error('Parameters must be positive'));

        expect(function () {
            new Board();
        }).toThrow(new Error('Parameters must be positive'));
    });

    it('should initialize a board', function () {
        var board = new Board(400, 400, 60);

        expect(board).toBeDefined();
        expect(board.tiles).toBeDefined();
        expect(board.oceanTiles).toBeDefined();
    });

    it('should properly distribute terrain', function () {
        var board = new Board(400, 400, 60);

        expect(board.tiles['1']).not.toBeDefined();
        expect(board.tiles['2'].length).toEqual(1);
        expect(board.tiles['3'].length).toEqual(2);
        expect(board.tiles['4'].length).toEqual(2);
        expect(board.tiles['5'].length).toEqual(2);
        expect(board.tiles['6'].length).toEqual(2);
        expect(board.tiles['7'].length).toEqual(1);
        expect(board.tiles['8'].length).toEqual(2);
        expect(board.tiles['9'].length).toEqual(2);
        expect(board.tiles['10'].length).toEqual(2);
        expect(board.tiles['11'].length).toEqual(2);
        expect(board.tiles['12'].length).toEqual(1);

        expect(board.oceanTiles.length).toEqual(18);

        var terrainCounts = {
            desert: 0,
            ore: 0,
            brick: 0,
            lumber: 0,
            wheat: 0,
            wool: 0
        };

        angular.forEach(board.tiles, function (roll) {
            angular.forEach(roll, function (tile) {

                terrainCounts[tile.terrain]++;

                // make sure the desert has 7
                if (tile.terrain == 'desert') {
                    expect(tile.roll).toEqual(7);

                } else {
                    expect(tile.roll).not.toEqual(7);
                    expect(tile.roll).toBeGreaterThan(1);
                    expect(tile.roll).toBeLessThan(13);
                }
            });
        });

        expect(terrainCounts.desert).toEqual(1);
        expect(terrainCounts.ore).toEqual(3);
        expect(terrainCounts.brick).toEqual(3);
        expect(terrainCounts.lumber).toEqual(4);
        expect(terrainCounts.wheat).toEqual(4);
        expect(terrainCounts.wool).toEqual(4);
    });

    it('should properly distribute rolls', function () {
        var board = new Board(400, 400, 60);

        var rollCounts = [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ];

        angular.forEach(board.tiles, function (roll) {
            angular.forEach(roll, function (tile) {
                rollCounts[tile.roll]++;

                // make sure the desert doesn't have a number
                if (tile.terrain == 'desert') {
                    expect(tile.roll).toEqual(7);

                } else {
                    expect(tile.roll).not.toEqual(7);
                    expect(tile.roll).toBeGreaterThan(1);
                    expect(tile.roll).toBeLessThan(13);
                }
            });
        });

        expect(rollCounts[0]).toEqual(0);
        expect(rollCounts[1]).toEqual(0);
        expect(rollCounts[2]).toEqual(1);
        expect(rollCounts[3]).toEqual(2);
        expect(rollCounts[4]).toEqual(2);
        expect(rollCounts[5]).toEqual(2);
        expect(rollCounts[6]).toEqual(2);
        expect(rollCounts[7]).toEqual(1);
        expect(rollCounts[8]).toEqual(2);
        expect(rollCounts[9]).toEqual(2);
        expect(rollCounts[10]).toEqual(2);
        expect(rollCounts[11]).toEqual(2);
        expect(rollCounts[12]).toEqual(1);
    });

    it('should create 6 plots for each tile', function () {
        var board = new Board(400, 400, 60);

        angular.forEach(board.tiles, function (roll) {
            angular.forEach(roll, function (tile) {
                expect(tile.plots.length).toEqual(6);
            });
        });
    });

    it('should re-use existing plots for shared plots', function () {
        var board = new Board(400, 400, 60);
        var firstTile = board.tiles[2][0];

        expect(board.plots.length).toEqual(54);

        // every tile has at least 4 shared plots, so check for all plots of a single tile
        // among all other tiles
        // for at least 6 matches (at a minimum, 2 of the 4 plots will be shared by 1 other tile
        // and 2 will be shared by 2 other tiles, so 1 + 1 + 2 + 2 = 6)
        var matches = 0;
        angular.forEach(board.tiles, function (roll) {
            angular.forEach(roll, function (tile) {
                if (tile != firstTile) {
                    angular.forEach(tile.plots, function (plot) {
                        angular.forEach(firstTile.plots, function (firstTilePlot) {
                            if ((plot.cx == firstTilePlot.cx) && (plot.cy == firstTilePlot.cy)) {
                                matches++;
                            }
                        });
                    });
                }
            });
        });

        expect(matches).toBeGreaterThan(3);
    });

    it('should make sure that the desert gets a 7', function () {
        var board = new Board(400, 400, 60);
        expect(board.tiles[7][0].terrain).toEqual('desert');
    });

    it('should assign neighbors to plots', function () {
        var board = new Board(400, 400, 60);

        angular.forEach(board.plots, function (plot) {
            expect(plot.neighbors).toBeDefined();
            expect(plot.neighbors.length).toBeGreaterThan(1);
            expect(plot.neighbors.length).toBeLessThan(4);
        });
    });

    it('should create paths between neighbors', function () {
        var board = new Board(400, 400, 60);

        expect(board.paths).toBeDefined();
        expect(board.paths.length).toEqual(72);
    });

    it('should make sure that the desert/7 starts with the robber', function () {
        var board = new Board(400, 400, 60);
        expect(board.tiles[7][0].hasRobber).toBe(true);
    });
});