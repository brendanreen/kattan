'use strict';

var board = angular.module('board', []);

board.config(['$routeProvider', function ($routeProvider) {

    $routeProvider.when('/board', {
        templateUrl: 'components/board/board.html',
        controller: 'BoardCtrl'
    });

}]);

board.controller('BoardCtrl',
    ['$scope', '$log', '$window', '$modal', 'boardService', 'buildService', 'gameService',
        function ($scope, $log, $window, $modal, boardService, buildService, gameService) {

            var highlightTile = function (tile) {
                tile.oldFill = tile.fill;
                tile.fill = 'purple';
            };

            var clearTile = function (tile) {
                if (tile.oldFill) {
                    tile.fill = tile.oldFill;
                }
            };

            var awardResources = function (tile) {
                if (tile.terrain != 'desert' && !tile.hasRobber) {
                    angular.forEach(tile.plots, function (plot) {
                        if (plot.hasSettlement) {
                            angular.forEach($scope.players, function (player) {
                                angular.forEach(player.settlements, function (settlement) {
                                    if (settlement == plot) {
                                        player.resources[tile.terrain]++;

                                        // if it's a city, award an extra resource
                                        if (plot.hasCity) {
                                            player.resources[tile.terrain]++;
                                        }
                                    }
                                });
                            });
                        }
                    });
                }
            };

            $scope.getRoll = function () {
                return Math.floor(Math.random() * 11) + 2;
            };

            $scope.rollDice = function () {
                if ($scope.isStartingPlacement()) {
                    throw new Error('Cannot roll during starting placement');

                } else if (!$scope.rolled) {
                    $scope.roll = $scope.getRoll();
                    $log.debug($scope.tiles[$scope.roll]);

                    angular.forEach($scope.tilesFlat, function (tile) {
                        clearTile(tile);
                    });

                    angular.forEach($scope.tiles[$scope.roll], function (tile) {
                        highlightTile(tile);
                        awardResources(tile);
                    });

                    if ($scope.roll == 7) {
                        $scope.robberMustMove = true;
                    }

                    $scope.rolled = true;
                }
            };

            var endTurn = function () {
                if ($scope.currentPlayer.points >= 10) {
                    $scope.winner = $scope.currentPlayer;

                } else {
                    $scope.currentPlayer = $scope.game.getNextPlayer();

                    if ($scope.isStartingPlacement()) {
                        $scope.showPlots = true;
                    }

                    angular.forEach($scope.tilesFlat, function (tile) {
                        clearTile(tile);
                    });

                    $scope.rolled = false;
                    $scope.building = false;
                    $scope.buildingCity = false;
                }
            };

            $scope.endTurn = function () {
                if ($scope.isStartingPlacement()) {
                    throw new Error('Cannot end turn during starting placement');

                } else if ($scope.robberMustMove) {
                    throw new Error('Robber must be moved first');

                } else {
                    endTurn();
                }
            };

            $scope.build = function (plot) {
                if ($scope.building || $scope.isStartingPlacement()) {
                    try {
                        if ($scope.buildingCity) {
                            buildService.buildCity($scope.currentPlayer, plot);
                            $scope.building = false;
                            $scope.buildingCity = false;

                        } else {
                            buildService.buildSettlement($scope.currentPlayer, plot, $scope.isStartingPlacement());
                            $scope.showPlots = false;
                            $scope.building = false;

                            if ($scope.isStartingResource()) {
                                angular.forEach($scope.tilesFlat, function (tile) {
                                    if (tile.terrain != 'desert') {
                                        angular.forEach(tile.plots, function (_plot) {
                                            if (_plot == plot) {
                                                $scope.currentPlayer.resources[tile.terrain]++;
                                            }
                                        });
                                    }
                                });
                            }

                            if ($scope.isStartingPlacement()) {
                                $scope.showPaths = true;
                            }
                        }
                    } catch (error) {
                        $log.info(error.message);
                        $scope.errorMessage = error.message;
                    }
                }
            };

            $scope.buildRoad = function (path) {
                if ($scope.building || $scope.isStartingPlacement()) {
                    try {
                        buildService.buildRoad($scope.currentPlayer, path);
                        $scope.showPaths = false;
                        $scope.building = false;

                        if ($scope.isStartingPlacement()) {
                            endTurn();
                        }

                    } catch (error) {
                        $log.info(error.message);
                        $scope.errorMessage = error.message;
                    }
                }
            };

            $scope.clearError = function () {
                $scope.errorMessage = null;
            };

            $scope.startBuildingRoad = function () {
                if ($scope.robberMustMove) {
                    throw new Error('Robber must be moved first');

                } else if ($scope.currentPlayer.canAffordRoad() && $scope.rolled) {
                    $scope.showPaths = true;
                    $scope.building = true;
                }
            };

            $scope.startBuildingSettlement = function () {
                if ($scope.robberMustMove) {
                    throw new Error('Robber must be moved first');

                } else if ($scope.currentPlayer.canAffordSettlement() && $scope.rolled) {
                    $scope.showPlots = true;
                    $scope.building = true;
                }
            };

            $scope.startBuildingCity = function () {
                if ($scope.robberMustMove) {
                    throw new Error('Robber must be moved first');

                } else if ($scope.currentPlayer.canAffordCity() && $scope.rolled) {
                    $scope.building = true;
                    $scope.buildingCity = true;
                }
            };

            $scope.buyDevelopmentCard = function () {
                if ($scope.robberMustMove) {
                    throw new Error('Robber must be moved first');
                }
            };

            $scope.cancelBuild = function () {
                $scope.showPaths = false;
                $scope.showPlots = false;
                $scope.building = false;
                $scope.buildingCity = false;
            };

            $scope.openTrade = function () {
                if ($scope.robberMustMove) {
                    throw new Error('Robber must be moved first');

                } else if ($scope.rolled) {
                    $scope.tradeModal = $modal.open({
                        templateUrl: 'components/trade/trade.html',
                        controller: 'TradeCtrl',
                        resolve: {
                            player: function () {
                                return $scope.currentPlayer;
                            }
                        }
                    });

                    $scope.tradeModal.result.then(function (trade) {
                        if (trade.type == 'bank') {
                            angular.forEach(trade.offer, function (amount, resource) {
                                $scope.currentPlayer.resources[resource] -= amount;
                            });

                            angular.forEach(trade.demand, function (amount, resource) {
                                $scope.currentPlayer.resources[resource] += amount;
                            });
                        }
                    }, function (type) {
                    });
                }
            };

            $scope.moveRobber = function (tile) {
                if ($scope.robberMustMove && !tile.hasRobber) {
                    angular.forEach($scope.tilesFlat, function (tile) {
                        tile.hasRobber = false;
                    });

                    tile.hasRobber = true;

                    $scope.robberMustMove = false;
                }
            };

// ---------------------------------------------------------------------------------------------------------
// ----- setup
// ---------------------------------------------------------------------------------------------------------

            var width = $window.innerWidth;
            var height = $window.innerHeight;

            $scope.graph = {
                width: width * 0.9,
                height: height * 0.9
            };

            var offsetX = 300;
            var offsetY = 400;
            var board = boardService.getBoard($scope.graph.width / 2 - offsetX, $scope.graph.height / 2 - offsetY, 50);

            $scope.tiles = board.tiles;
            $scope.oceanTiles = board.oceanTiles;
            $scope.plots = board.plots;
            $scope.paths = board.paths;

            $log.debug($scope.tiles);

            $scope.tilesFlat = [];
            angular.forEach($scope.tiles, function (roll) {
                angular.forEach(roll, function (tile) {
                    $scope.tilesFlat.push(tile);
                });
            });

            $scope.showPlots = true;
            $scope.showPaths = false;

            gameService.newGame({ numPlayers: 3 }).then(function (game) {
                $scope.game = game;
                $scope.players = $scope.game.players;
                $scope.currentPlayer = $scope.game.getCurrentPlayer();

                $scope.isStartingPlacement = function () {
                    return $scope.game.isStartingPlacement();
                };

                $scope.isStartingResource = function () {
                    return $scope.game.isStartingResource();
                };
            });

            $scope.rolled = false;
        }]);