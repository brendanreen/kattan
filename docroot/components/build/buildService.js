'use strict';

app.factory('buildService', ['$log', function ($log) {

    return {

        buildSettlement: function (player, plot, isStartingPlacement) {

            if (plot.hasSettlement) {
                throw new Error('Cannot build settlement on occupied plot');

            } else if (!player.canAffordSettlement()) {
                throw new Error('Insufficient resources');

            } else {
                var hasNeighboringSettlement = false;
                angular.forEach(plot.neighbors, function (neighbor) {
                    hasNeighboringSettlement = hasNeighboringSettlement || neighbor.hasSettlement;
                });

                if (hasNeighboringSettlement) {
                    throw new Error('Cannot build settlement on plot with neighbors');
                }

                else {
                    var hasAdjacentRoad = false;
                    angular.forEach(plot.paths, function (path) {
                        hasAdjacentRoad = hasAdjacentRoad || (path.hasRoad && player.roads.indexOf(path) != -1);
                    });

                    if (!isStartingPlacement && !hasAdjacentRoad) {
                        throw new Error('Must build settlement next to an existing road');

                    } else {
                        plot.hasSettlement = true;
                        plot.fill = player.color;
                        player.addSettlement(plot);
                    }
                }
            }
        },

        buildRoad: function (player, path) {
            if (!player.canAffordRoad()) {
                throw new Error('Insufficient resources');

            } else {
                var connectedToSettlement = false;
                var connectedToRoad = false;

                angular.forEach(player.settlements, function (plot) {
                    connectedToSettlement = connectedToSettlement || plot == path.plotA || plot == path.plotB;
                });

                angular.forEach(player.roads, function (pathA) {

                    angular.forEach(path.plotA.paths, function (pathB) {
                        connectedToRoad = connectedToRoad || pathA == pathB;
                    });

                    angular.forEach(path.plotB.paths, function (pathB) {
                        connectedToRoad = connectedToRoad || pathA == pathB;
                    });
                });

                if (!connectedToSettlement && !connectedToRoad) {
                    throw new Error('Road must be connected to a settlement or a road');
                }

                else {
                    path.hasRoad = true;
                    path.fill = player.color;
                    player.addRoad(path);
                }
            }
        },

        buildCity: function (player, plot) {
            if (!plot.hasSettlement || plot.hasCity || player.settlements.indexOf(plot) == -1) {
                throw new Error('Must build city on an existing settlement');

            } else if (!player.canAffordCity()) {
                throw new Error('Insufficient resources');

            } else {
                plot.hasCity = true;
                plot.strokeWidth = 2;
                plot.stroke = 'lavender';
                plot.r = 24;
                player.addCity(plot);
            }
        }
    };
}]);