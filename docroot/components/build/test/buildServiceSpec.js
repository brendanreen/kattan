'use strict';

describe('buildService', function () {

    var buildService;
    var board;
    var player1, player2;

    var noResources = {
        brick: 0,
        lumber: 0,
        wheat: 0,
        wool: 0,
        ore: 0
    };

    var playerData = {
        id: 0,
        points: 0,
        resources: {
            brick: 4,
            lumber: 4,
            wool: 2,
            wheat: 2,
            ore: 0
        },
        settlements: [],
        roads: [],
        developmentCards: [],
        color: 'red',
        name: 'ernesto'
    };

    beforeEach(module('app'));

    beforeEach(inject(function (_buildService_) {
        buildService = _buildService_;
        board = new Board(400, 400, 50);
        player1 = new Player(playerData);
        player2 = new Player(playerData);
    }));

    describe('settlements', function () {

        it('should build a settlement on an unoccupied plot with no neighbors', function () {
            var plot = board.plots[0];
            expect(plot.hasSettlement).toBeFalsy();

            buildService.buildSettlement(player1, plot, true);
            expect(plot.hasSettlement).toBe(true);
            expect(player1.settlements).toEqual([plot]);
        });

        it('should not build a settlement on an occupied plot', function () {
            var plot = board.plots[0];

            buildService.buildSettlement(player1, plot, true);
            expect(function () {
                buildService.buildSettlement(player1, plot, true);
            }).toThrow(new Error('Cannot build settlement on occupied plot'));
            expect(player1.settlements).toEqual([plot]);
        });

        it('should not build a settlement on an unoccupied plot with neighbors', function () {
            expect(function () {
                angular.forEach(board.plots, function (plot) {
                    buildService.buildSettlement(player1, plot, true);
                });
            }).toThrow(new Error('Cannot build settlement on plot with neighbors'));
        });

        it('should not build a settlement if the player has insufficient resources', function () {
            var plot = board.plots[0];

            player1.resources = angular.copy(noResources);

            expect(function () {
                buildService.buildSettlement(player1, plot, true);
            }).toThrow(new Error('Insufficient resources'));
        });

        it('unless in starting placement, should not allow you to build a settlement unattached to an existing road',
            function () {
                var plot = board.plots[0];

                expect(function () {
                    buildService.buildSettlement(player1, plot, false);
                }).toThrow(new Error('Must build settlement next to an existing road'));
            });
    });

    describe('roads', function () {

        it('should build a road on an unoccupied path if that player owns an adjacent settlement', function () {
            var plot = board.plots[0];
            buildService.buildSettlement(player1, plot, true);

            var pathToBuild;
            angular.forEach(board.paths, function (path) {
                if ((path.plotA.cx == plot.cx && path.plotA.cy == plot.cy) ||
                    (path.plotB.cx == plot.cx && path.plotB.cy == plot.cy)) {
                    pathToBuild = path;
                }
            });

            expect(pathToBuild).toBeDefined();
            expect(pathToBuild.hasRoad).toBeFalsy();

            buildService.buildRoad(player1, pathToBuild);
            expect(pathToBuild.hasRoad).toBe(true);
            expect(player1.roads).toEqual([pathToBuild]);
        });

        it('should build a road on an unoccupied path if that player owns an adjacent road', function () {
            var plot = board.plots[0];
            buildService.buildSettlement(player1, plot, true);

            var pathA, pathB;
            angular.forEach(board.paths, function (path) {
                // only match on plotA so we can use plotB as the starting point for the second road
                if (path.plotA.cx == plot.cx && path.plotA.cy == plot.cy) {
                    pathA = path;
                }
            });

            buildService.buildRoad(player1, pathA);
            expect(player1.roads).toEqual([pathA]);

            angular.forEach(board.paths, function (path) {
                if (path.plotB.cx == pathA.plotB.cx && path.plotB.cy == pathA.plotB.cy) {
                    pathB = path;
                }
            });

            expect(pathB).toBeDefined();
            expect(pathB.hasRoad).toBeFalsy();

            buildService.buildRoad(player1, pathB);
            expect(pathB.hasRoad).toBe(true);
            expect(player1.roads).toEqual([pathA, pathB]);
        });

        it('should not build a road on an unoccupied road if there is an not adjacent settlement or road', function () {
            var pathToBuild = board.paths[0];
            expect(pathToBuild).toBeDefined();
            expect(pathToBuild.hasRoad).toBeFalsy();

            expect(function () {
                buildService.buildRoad(player1, pathToBuild);
            }).toThrow(new Error('Road must be connected to a settlement or a road'));
            expect(player1.roads.length).toBe(0);
        });

        it('should not build a road on an unoccupied path if that player does not own an adjacent settlement',
            function () {
                var plot = board.plots[0];
                buildService.buildSettlement(player1, plot, true);

                var pathToBuild;
                angular.forEach(board.paths, function (path) {
                    if ((path.plotA.cx == plot.cx && path.plotA.cy == plot.cy) ||
                        (path.plotB.cx == plot.cx && path.plotB.cy == plot.cy)) {
                        pathToBuild = path;
                    }
                });

                expect(pathToBuild).toBeDefined();
                expect(pathToBuild.hasRoad).toBeFalsy();

                expect(function () {
                    buildService.buildRoad(player2, pathToBuild);
                }).toThrow(new Error('Road must be connected to a settlement or a road'));

                expect(pathToBuild.hasRoad).toBeFalsy();
                expect(player1.roads).toEqual([]);
                expect(player2.roads).toEqual([]);
            });

        it('should not build a road if the player has insufficient resources', function () {
            var plot = board.plots[0];

            player1.resources = angular.copy(noResources);
            player1.canAffordSettlement = function () {
                return true;
            };

            buildService.buildSettlement(player1, plot, true);

            var pathToBuild;
            angular.forEach(board.paths, function (path) {
                if ((path.plotA.cx == plot.cx && path.plotA.cy == plot.cy) ||
                    (path.plotB.cx == plot.cx && path.plotB.cy == plot.cy)) {
                    pathToBuild = path;
                }
            });

            expect(function () {
                buildService.buildRoad(player1, pathToBuild);
            }).toThrow(new Error('Insufficient resources'));
        });
    });

    describe('cities', function () {

        it('should build a city on an existing settlement owned by the player', function () {
            var plot = board.plots[0];
            buildService.buildSettlement(player1, plot, true);

            player1.resources.ore += 3;
            player1.resources.wheat += 2;

            buildService.buildCity(player1, plot);
            expect(plot.hasCity).toBe(true);
        });

        it('should not build a city on an unoccupied plot', function () {
            var plot = board.plots[0];
            player1.resources.ore += 3;
            player1.resources.wheat += 2;

            expect(function () {
                buildService.buildCity(player1, plot);
            }).toThrow(new Error('Must build city on an existing settlement'));
            expect(plot.hasCity).toBeFalsy();
        });

        it('should not build a city on a settlement owned by another player', function () {
            var plot = board.plots[0];
            buildService.buildSettlement(player1, plot, true);

            player2.resources.ore += 3;
            player2.resources.wheat += 2;

            expect(function () {
                buildService.buildCity(player2, plot);
            }).toThrow(new Error('Must build city on an existing settlement'));
            expect(plot.hasCity).toBeFalsy();
        });

        it('should not build a city if the player has insufficient resources', function () {
            var plot = board.plots[0];
            buildService.buildSettlement(player1, plot, true);

            expect(function () {
                buildService.buildCity(player1, plot);
            }).toThrow(new Error('Insufficient resources'));
            expect(plot.hasCity).toBeFalsy();
        });

        it('should not build a city if a city already exists in that plot', function () {
            var plot = board.plots[0];
            buildService.buildSettlement(player1, plot, true);

            player1.resources.ore += 6;
            player1.resources.wheat += 4;

            buildService.buildCity(player1, plot);

            var resourcesBefore = angular.copy(player1.resources);
            expect(function () {
                buildService.buildCity(player1, plot);
            }).toThrow(new Error('Must build city on an existing settlement'));
            expect(player1.resources).toEqual(resourcesBefore);
        });
    });
});