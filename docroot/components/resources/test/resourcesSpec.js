'use strict';

describe('resources', function () {

    var elem;
    var $scope;

    var playerData = {
        id: 0,
        points: 0,
        resources: {
            brick: 4,
            lumber: 4,
            wool: 2,
            wheat: 2,
            ore: 0
        },
        settlements: [],
        roads: [],
        developmentCards: [],
        color: 'red',
        name: 'ernesto'
    };

    beforeEach(module('app'));

    beforeEach(inject(function ($rootScope, $compile) {
        elem = angular.element('<div data-resources data-resource-counts="currentPlayer.resources"></div>');

        $scope = $rootScope.$new();
        $scope.currentPlayer = new Player(playerData);

        $compile(elem)($scope);
        $scope.$digest();
    }));

    it('should create a div for each resource type', function () {
        var divs = elem.find('div');
        expect(divs.length).toBe(5);
        angular.forEach(divs, function (div, i) {
            expect(divs.eq(i).hasClass('resource-list')).toBe(true);
        });
    });

    it('should create a span for each resource of that type', function () {
        var divs = elem.find('div');
        var spans;

        // brick
        spans = divs.eq(0).find('span');
        expect(spans.length).toBe(4);
        angular.forEach(spans, function (span, i) {
            expect(spans.eq(i).hasClass('brick')).toBe(true);
        });

        // lumber
        spans = divs.eq(1).find('span');
        expect(spans.length).toBe(4);
        angular.forEach(spans, function (span, i) {
            expect(spans.eq(i).hasClass('lumber')).toBe(true);
        });

        // ore
        spans = divs.eq(2).find('span');
        expect(spans.length).toBe(0);
        angular.forEach(spans, function (span, i) {
            expect(spans.eq(i).hasClass('ore')).toBe(true);
        });

        // wheat
        spans = divs.eq(3).find('span');
        expect(spans.length).toBe(2);
        angular.forEach(spans, function (span, i) {
            expect(spans.eq(i).hasClass('wheat')).toBe(true);
        });

        // wool
        spans = divs.eq(4).find('span');
        expect(spans.length).toBe(2);
        angular.forEach(spans, function (span, i) {
            expect(spans.eq(i).hasClass('wool')).toBe(true);
        });
    });

    it('should update display when the model changes', function () {
        var divs = elem.find('div');
        var spans = divs.eq(0).find('span');
        expect(spans.length).toBe(4);

        $scope.currentPlayer.resources.brick++;
        $scope.$apply();
        spans = divs.eq(0).find('span');
        expect(spans.length).toBe(5);

        $scope.currentPlayer.resources.brick--;
        $scope.$apply();
        spans = divs.eq(0).find('span');
        expect(spans.length).toBe(4);
    });

});