'use strict';

app.directive('resources', ['$log', function ($log) {

    return {
        restrict: 'A',
        template: '<div class="resource-list" data-ng-repeat="(resource, rs) in resourceIndicators">' +
            '<span data-ng-class="resource" data-ng-repeat="r in rs" ' +
            'data-ng-click="selectResource({resource: resource})">&#9608; </span>' +
            '</div>',
        scope: {
            resourceCounts: '=',
            selectResource: '&'
        },

        link: function (scope, elem, attrs) {

            var updateResourceIndicators = function () {
                scope.resourceIndicators = {
                    lumber: [],
                    brick: [],
                    wool: [],
                    wheat: [],
                    ore: []
                };

                if (scope.resourceCounts) {
                    angular.forEach(Object.keys(scope.resourceCounts), function (resource) {
                        for (var i = 0; i < scope.resourceCounts[resource]; i++) {
                            scope.resourceIndicators[resource].push({});
                        }
                    });
                }
            };

            scope.$watch('resourceCounts', function () {
                updateResourceIndicators();
            }, true);
        }
    }
}]);