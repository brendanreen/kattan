'use strict';

describe('playerService', function () {

    var playerService;
    var $httpBackend;

    var startingResources = {
        brick: 4,
        lumber: 4,
        wheat: 2,
        wool: 2,
        ore: 0
    };

    beforeEach(module('app'));

    beforeEach(inject(function (_playerService_, _$httpBackend_) {
        playerService = _playerService_;
        $httpBackend = _$httpBackend_;
        $httpBackend.whenGET('/api/player').respond({
            points: 0,
            resources: startingResources,
            developmentCards: []
        });
    }));

    afterEach(function () {
        $httpBackend.verifyNoOutstandingExpectation();
        $httpBackend.verifyNoOutstandingRequest();
    });

    it('should create a new player', function () {
        $httpBackend.expectGET('/api/player');
        playerService.newPlayer().then(function (player) {
            expect(player.points).toEqual(0);
            expect(player.resources).toEqual(startingResources);
            expect(player.developmentCards).toEqual([]);
        });
        $httpBackend.flush();
    });
});