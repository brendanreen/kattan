'use strict';

describe('player', function () {

    var noResources = {
        brick: 0,
        lumber: 0,
        wheat: 0,
        wool: 0,
        ore: 0
    };

    var playerData = {
        id: 0,
        points: 0,
        resources: {
            brick: 4,
            lumber: 4,
            wool: 2,
            wheat: 2,
            ore: 0
        },
        settlements: [],
        roads: [],
        developmentCards: [],
        color: 'red',
        name: 'ernesto'
    };

    var player;

    beforeEach(module('app'));

    beforeEach(function () {
        player = new Player(playerData);
    });

    it('should know when a player can afford at least 1 thing', function () {
        expect(player.canAffordAnything()).toBe(true);

        player.resources = angular.copy(noResources);
        expect(player.canAffordAnything()).toBe(false);

        player.resources.lumber = 1;
        player.resources.brick = 1;
        expect(player.canAffordAnything()).toBe(true);

        player.resources = angular.copy(noResources);
        player.resources.wool = 1;
        player.resources.wheat = 1;
        player.resources.ore = 1;
        expect(player.canAffordAnything()).toBe(true);

        player.resources = angular.copy(noResources);
        player.resources.wheat = 2;
        player.resources.ore = 3;
        expect(player.canAffordAnything()).toBe(true);
    });

    it('should correctly calculate number of resources player has', function () {
        expect(player.numResources()).toBe(12);

        player.resources = angular.copy(noResources);
        expect(player.numResources()).toBe(0);
    });

    it('should decrement appropriate resources if a road is built', function () {
        var resourcesBefore = angular.copy(player.resources);

        player.addRoad({});

        resourcesBefore.brick--;
        resourcesBefore.lumber--;
        expect(player.resources).toEqual(resourcesBefore);
    });

    it('should decrement appropriate resources if a settlement is built', function () {
        var resourcesBefore = angular.copy(player.resources);

        player.addSettlement({});

        resourcesBefore.brick--;
        resourcesBefore.lumber--;
        resourcesBefore.wheat--;
        resourcesBefore.wool--;
        expect(player.resources).toEqual(resourcesBefore);
    });

    it('should decrement appropriate resources if a city is built', function () {
        var resourcesBefore = angular.copy(player.resources);

        player.addCity({});

        resourcesBefore.wheat -= 2;
        resourcesBefore.ore -= 3;
        expect(player.resources).toEqual(resourcesBefore);
    });

    it('should award a point if a settlement is built', function () {
        player.addSettlement({});
        expect(player.points).toBe(1);
    });

    it('should award a point if a city is built', function () {
        player.addSettlement({});
        player.addCity({});
        expect(player.points).toBe(2);
    });
});