'use strict';

app.factory('playerService', ['$log', '$http', '$q', function ($log, $http, $q) {

    return {
        newPlayer: function () {
            var playerDeferred = $q.defer();
            $http.get('/api/player').then(function (response) {
                playerDeferred.resolve(new Player(response.data));
            });
            return playerDeferred.promise;
        }
    };
}]);