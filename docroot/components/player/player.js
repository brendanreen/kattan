var Player = function (playerData) {
    this.id = playerData.id;
    this.points = playerData.points;
    this.color = playerData.color;
    this.name = playerData.name;
    this.resources = angular.copy(playerData.resources);
    this.settlements = angular.copy(playerData.settlements);
    this.roads = angular.copy(playerData.roads);
    this.developmentCards = angular.copy(playerData.developmentCards);
};

Player.prototype = {

    constructor: Player,

    canAffordSettlement: function () {
        return this.resources.brick >= 1 &&
            this.resources.lumber >= 1 &&
            this.resources.wheat >= 1 &&
            this.resources.wool >= 1;
    },

    canAffordRoad: function () {
        return this.resources.brick >= 1 &&
            this.resources.lumber >= 1;
    },

    canAffordDevelopmentCard: function () {
        return this.resources.wool >= 1 &&
            this.resources.wheat >= 1 &&
            this.resources.ore >= 1;
    },

    canAffordCity: function () {
        return this.resources.wheat >= 2 &&
            this.resources.ore >= 3;
    },

    addSettlement: function (plot) {
        this.settlements.push(plot);
        this.resources.brick--;
        this.resources.lumber--;
        this.resources.wheat--;
        this.resources.wool--;
        this.points++;
    },

    addRoad: function (path) {
        this.roads.push(path);
        this.resources.brick--;
        this.resources.lumber--;
    },

    addCity: function (plot) {
        this.resources.ore -= 3;
        this.resources.wheat -= 2;
        this.points++;
    },

    canAffordAnything: function () {
        return this.canAffordSettlement() ||
            this.canAffordRoad() ||
            this.canAffordDevelopmentCard() ||
            this.canAffordCity();
    },

    numResources: function () {
        var numResources = 0;
        angular.forEach(this.resources, function (amount) {
            numResources += amount;
        });
        return numResources;
    }

};