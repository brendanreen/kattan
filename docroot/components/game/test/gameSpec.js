'use strict';

describe('game', function () {

    var playerData = {
        id: 0,
        points: 0,
        resources: {
            brick: 4,
            lumber: 4,
            wool: 2,
            wheat: 2,
            ore: 0
        },
        settlements: [],
        roads: [],
        developmentCards: [],
        color: 'red',
        name: 'ernesto'
    };

    var game;

    beforeEach(module('app'));

    beforeEach(function () {
        var gameData = {
            players: [new Player(playerData), new Player(playerData), new Player(playerData)]
        };
        game = new Game(gameData);
    });

    it('should determine whose turn it is', function () {
        expect(game.getNextPlayer()).toBeDefined();
    });

    it('should follow the starting placement turn sequence and then the normal turn sequence', function () {
        var players = game.players;

        // placement sequence
        expect(game.getCurrentPlayer()).toEqual(players[0]);
        expect(game.getNextPlayer()).toEqual(players[1]);
        expect(game.getNextPlayer()).toEqual(players[2]);
        expect(game.isStartingResource()).toBe(false);
        expect(game.getNextPlayer()).toEqual(players[2]);
        expect(game.isStartingResource()).toBe(true);
        expect(game.getNextPlayer()).toEqual(players[1]);
        expect(game.isStartingResource()).toBe(true);
        expect(game.getNextPlayer()).toEqual(players[0]);

        // normal sequence
        expect(game.getNextPlayer()).toEqual(players[0]);
        expect(game.isStartingPlacement()).toBe(false);
        expect(game.getNextPlayer()).toEqual(players[1]);
        expect(game.getNextPlayer()).toEqual(players[2]);
        expect(game.getNextPlayer()).toEqual(players[0]);
        expect(game.getNextPlayer()).toEqual(players[1]);
        expect(game.getNextPlayer()).toEqual(players[2]);
    });

    it('should indicate whether or not it is currently in the starting placement phase', function () {
        expect(game.isStartingPlacement()).toBe(true); // 0
        game.getNextPlayer(); // 1
        game.getNextPlayer(); // 2
        game.getNextPlayer(); // 2
        game.getNextPlayer(); // 1
        game.getNextPlayer(); // 0
        game.getNextPlayer(); // 0
        expect(game.isStartingPlacement()).toBe(false);
    });
});