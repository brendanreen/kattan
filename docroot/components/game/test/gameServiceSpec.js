'use strict';

describe('gameService', function () {

    var gameService;
    var $httpBackend;

    beforeEach(module('app'));

    beforeEach(inject(function (_gameService_, _$httpBackend_) {
        gameService = _gameService_;
        $httpBackend = _$httpBackend_;
        $httpBackend.whenGET('/api/game/3').respond({
            players: [{}, {}, {}]
        });
    }));

    afterEach(function () {
        $httpBackend.verifyNoOutstandingExpectation();
        $httpBackend.verifyNoOutstandingRequest();
    });

    it('should not create a game with less than 3 players', function () {
        expect(function () {
            gameService.newGame();
        }).toThrow(new Error('Cannot create game with less than 3 players'));

        expect(function () {
            gameService.newGame({ numPlayers: 2 });
        }).toThrow(new Error('Cannot create game with less than 3 players'));
    });

    it('should not create a game with more than 4 players', function () {
        expect(function () {
            gameService.newGame({ numPlayers: 5 });
        }).toThrow(new Error('Cannot create game with more than 4 players'));
    });

    it('should create a game with 3 players', inject(function ($rootScope) {
        $httpBackend.expectGET('/api/game/3');
        gameService.newGame({ numPlayers: 3 }).then(function (game) {
            expect(game).toBeDefined();
            expect(game.players.length).toEqual(3);
        });
        $httpBackend.flush();
    }));
});