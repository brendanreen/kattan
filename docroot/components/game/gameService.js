'use strict';

app.factory('gameService', ['$log', '$q', '$http', function ($log, $q, $http) {

    return {
        newGame: function (params) {
            if (!params || !params.numPlayers || params.numPlayers < 3) {
                throw new Error('Cannot create game with less than 3 players');
            }

            else if (params.numPlayers > 4) {
                throw new Error('Cannot create game with more than 4 players');
            }

            else {
                var gameDeferred = $q.defer();
                $http.get('/api/game/' + params.numPlayers).then(function (response) {
                    gameDeferred.resolve(new Game(response.data));
                });
                return gameDeferred.promise;
            }
        }
    };
}]);