var Game = function (gameData) {
    var players = [];
    angular.forEach(gameData.players, function (playerData) {
        var player = new Player(playerData);
        players.push(player);
    });

    this.players = players;
    this.currentPlayer = players[0];
    this.placementOrder = [];

    var i;
    for (i = 0; i < players.length; i++) {
        this.placementOrder.push(players[i]);
    }

    for (i = players.length - 1; i > 0; i--) {
        this.placementOrder.push(players[i]);
    }

    this.placementOrder.unshift(players[0]);
};

Game.prototype = {

    constructor: Game,

    getCurrentPlayer: function () {
        return this.currentPlayer;
    },

    getNextPlayer: function () {
        // if we're still in the placement phase, follow that player sequence
        if (this.placementOrder.length > 0) {
            this.currentPlayer = this.placementOrder.pop();
        }

        // otherwise, follow the normal sequence
        else {
            var index = this.players.indexOf(this.currentPlayer) + 1;
            index = index % this.players.length;
            this.currentPlayer = this.players[index];
        }

        return this.currentPlayer;
    },

    isStartingPlacement: function () {
        return this.placementOrder.length > 0;
    },

    isStartingResource: function () {
        return this.placementOrder.length <= 3 && this.placementOrder.length > 0;
    }
};