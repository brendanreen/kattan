'use strict';

var Player = require('../player');

describe('player', function () {

    var startingResources = {
        brick: 4,
        lumber: 4,
        wheat: 2,
        wool: 2,
        ore: 0
    };

    it('should create a new player', function () {
        var player = new Player();
        expect(player).toBeDefined();
        expect(player.points).toEqual(0);
        expect(player.resources).toEqual(startingResources);
        expect(player.developmentCards).toEqual([]);
        expect(player.color).toBeDefined();
    });

    it('should give players unique ids', function () {
        var player1 = new Player();
        var player2 = new Player();
        expect(player1.id).not.toEqual(player2.id);
    });
});