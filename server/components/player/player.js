var id = 0;
var getId = function () {
    return id++;
};

var names = ['hans', 'greta', 'dietrich', 'günther'];

var colors = ['red', 'blue', 'orange', 'orchid'];

var Player = function () {
    this.id = getId();
    this.points = 0;
    this.resources = {
        brick: 4,
        lumber: 4,
        wool: 2,
        wheat: 2,
        ore: 0
    };
    this.settlements = [];
    this.roads = [];
    this.developmentCards = [];
    this.color = colors[this.id % colors.length];
    this.name = names[this.id % names.length];
};

module.exports = Player;