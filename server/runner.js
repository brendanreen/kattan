var jasmine = require('jasmine-node');

jasmine.executeSpecsInFolder({
    specFolders: ['components'],
    isVerbose: true,
    showColors: true
});