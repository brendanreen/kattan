var express = require('express');
var app = express();
var api = require('./routes/api');

app.set("view options", { layout: false });
app.use(express.static(__dirname + '/../docroot'));

app.get('/', function (req, res) {
    res.render('index.html');
});

// api
app.get('/api/player', api.newPlayer);
app.get('/api/game/:numPlayers', api.newGame);

var server = app.listen(1337, function () {
    console.log('Listening on port %d', server.address().port);
});