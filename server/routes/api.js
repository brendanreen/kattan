var Player = require('../components/player/player');

exports.newPlayer = function (req, res) {
    res.json(new Player());
};

exports.newGame = function (req, res) {
    var numPlayers = req.params.numPlayers;

    if (numPlayers >= 3 && numPlayers <= 4) {
        res.json({ players: [new Player(), new Player(), new Player()] });

    } else {
        res.send(400);
    }
};